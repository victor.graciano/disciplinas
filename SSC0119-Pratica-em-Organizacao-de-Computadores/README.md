# SSC0119-Pratica-em-Organizacao-de-Computadores
Disciplina SSC0119 Prática em Organização de Computadores

- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@icmc.usp.br  (Obs.: só tem um @)
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável

## Processador ICMC - https://github.com/simoesusp/Processador-ICMC

- Nossa disciplina irá usar um Processador desenvolvido pelos próprios alunos do ICMC disponível neste repositório do github

## Simulador para programação em Assembly 
- Nossa disciplina irá usar um simulador para desenvolver programas em linguagem Assembly que poderá ser encontrado para Windows, Linux e MacOS em: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/
- Para Windows, fiz um link super fácil de instalar: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/Simulador_Windows_Tudo_Pronto_F%C3%A1cil%20(1).zip
  - Esse zip já vem inclusive com o sublime configurado para escrever o software (incluindo a sintaxe highlight) e o montador e o simulador já configurado para ser chamado com a tecla F7
  - Para instalar basta fazer o download na area de trabalho ou na pasta Documentos
  - Entrar na pasta ..\Simulador\Sublime Text 3
  - Executar o sublime: "sublime_text.exe"
  - Se ele pedir, NÃ0 FAÇA O UPDATE !!!!!!!!!!!!!!!
  - Vá em File - Open File e volte uma pasta para ..\Simulador\
  - Abra o sw em Assembly chamado Hello4.ASM
  - Teste se está tudo funcionando chamando o MONTADOR e o SIMULADOR com a tecla F7
  - Apartir daí pode-se salvar o sw com outro nome e fazer novos programas
  - Apenas preste atenção para estar na pasta ..\Simulador\
  - Se der o erro: [Decode error - output not utf-8] é porque você não está na pasta ..\Simulador\

## Alunos de 2021 - Primeiro semestre
- Lista de Presença - Favor assinar a Lista de Presença com seu Número USP durante o horario das aulas - https://docs.google.com/spreadsheets/d/1IHUSf2omdOpYeprRbMBOx1dbE4oKjMQ74UcrFWjPN2g/edit?usp=sharing
- Você pode acompanhar sua Frequência aqui: https://docs.google.com/spreadsheets/d/16fM6a5DsieDe4c5T0agJJdNC5QxwKWeIZlOhj1m6NwU/edit?usp=sharing
- As aulas começam dia 16/04/2021 e serão transmitidas pelo google Meet (sempre com o código meet.google.com/get-ygkf-zgz) no horário normal da disciplina: Sex. 16:20/18:00h
- As aulas serâo gravadas e disponibilisadas nessa plataforma
- Essa disciplina irá utilizar o projeto do Processador ICMC, que é um processador RISC de 16 bits implementado em FPGA - todas as ferramentas de software e hardware estão disponíveis no Github do projeto: https://github.com/simoesusp/Processador-ICMC

## Avaliação
- A avaliação será por meio da apresentação de um trabalho - Implementação de um Jogo em Linguagem Montadora (assembly) no processador ICMC (https://github.com/simoesusp/Processador-ICMC/tree/master/Processor_FPGA) ou no seu simulador físico (https://github.com/simoesusp/Processador-ICMC/tree/master/Simple_Simulator)
- Os alunos serão divididos em grupos de 2-3 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

### Apresentação do trabalho Turma 2021 - Primeiro semestre:
- Apresentação dos trabalhos pelo Google Meet será na SEMANA 26-30 de Julho: a ser informado o link

- Insira seu projeto e reserve um horario para apresentar no arquivo do GoogleDocs (precisa estar logado na USP), informando: TÍTULO DO PROJETO - nomes dos alunos - link pro github/gitlab - LINK DOCS: https://docs.google.com/document/d/10ftVO0WbTc1nRY5Sy94uFa-BbKCLz_LSNSHs6d5Ngos/edit?usp=sharing

# Projetos dos Alunos 2021
- Criar uma conta sua no Github/gitlab
- Os projetos devem conter um Readme explicando o projeto e o software deve estar muito bem comentado!!
- Incluir no seu Github/gitlab: o Código do Simulador do Processador modificado, o JOGO.ASM e o CPURAM.MIF, e caso tenha alterado, o CHARMAP.MIF. 
- Obrigatório: incluir um vídeo mostrando o jogo funcionando e explicando as modificações que fez no Processador. (Upa no Youtube e poe um link no Readme do teu Github/guitlab).
- Além do VÍDEO DE VOCË explicando o projeto, TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE por google Meet: a ser informado o link

## Minicurso OpenGL do Breno - https://github.com/Brenocq/OpenGL-Tutorial


# Aulas a Distância (2021 - Primeiro Semestre)

- O Material usado nas aulas está na pasta MaterialAulaDistancia

- Aula01 16/04/2021 (16:20h) - https://drive.google.com/file/d/1Bgw7v4G2tcryxD6RC8bhKQfjIoB7jpKu/view?usp=sharing

- Aula02 23/04/2021 (16:20h) - https://drive.google.com/file/d/17kQHaB83eE9A-oUNBYiPEWSCe-TGzH0F/view?usp=sharing

- Aula03 30/04/2021 (16:20h) - https://drive.google.com/file/d/1JW5gZ4LY6g-pu0K_WHYNMj-AMJrH00y2/view?usp=sharing

- Aula04 07/05/2021 (16:20h) - https://drive.google.com/file/d/1FX5-CeRvyuZwyf0XcUx6uek84lua1TvW/view?usp=sharing

- Aula05 14/05/2021 (16:20h) - https://drive.google.com/file/d/1Xhit1h19txz2XjqDHVJBHgM_d3N54a2B/view?usp=sharing
  - Explicação muito simples sobre JMP e BRANCH - https://en.wikibooks.org/wiki/Microprocessor_Design/Program_Counter

- Aula06 21/05/2021 (16:20h) - https://drive.google.com/file/d/1TrbT_Sef5_9rWwrpavi8hfBN83UMoyR-/view?usp=sharing

- Aula07 28/05/2021 (16:20h) - https://drive.google.com/file/d/1eatRDyZWmGQD4X9kU_51daoYY3k2DFyC/view?usp=sharing

- Aula08 11/06/2021 (16:20h) - https://drive.google.com/file/d/1ftWsAGOG_uY9l64u7407kzICY-u7Ybjg/view?usp=sharing

- Aula09 18/06/2021 (16:20h) - https://drive.google.com/file/d/1YjKzjqStwjoHxIE-UtzQX7TN5EqnMrPG/view?usp=sharing

- Aula10 25/06/2021 (16:20h) - Link para o Google Meet - meet.google.com/get-ygkf-zgz



## Aulas a Distância 2020 - Primeiro Semestre

- Minicurso OpenGL do Breno - https://github.com/Brenocq/OpenGL-Tutorial

- Aula01 - https://drive.google.com/file/d/1ucFPXroi7lXQq5qj4DN_a59JgJtiMLZR/view?usp=sharing

- Aula02 - https://drive.google.com/file/d/1okiL76FewUbyGMTcjNSI0Xhy0rsizia2/view?usp=sharing

- Aula03 17/04/20 - Parte1: https://drive.google.com/file/d/1JugSodvm6wiZIyiCYXl-gaDbAdB2Yd1P/view?usp=sharing - 
Parte2: https://drive.google.com/file/d/15Olmivl_6dFfzbz0j2-lQUj2PCYKGOXX/view?usp=sharing

- Aula04 24/04/20 - https://drive.google.com/file/d/1UgvGZ4suo8emRTWnyZTSAoGBYFHCAPB-/view?usp=sharing

- Aula05 08/05/20 - https://drive.google.com/file/d/1aMhA7_8POKLhRMsvV9SxC-DVYDXbCiBP/view?usp=sharing

- Aula06 15/05/20 - https://drive.google.com/file/d/1V-i44oPmlpRQ5K5WAYF6ywZH5auTft93/view?usp=sharing

- Aula07 20/05/20 - https://drive.google.com/file/d/1DRlHb2YtotdG1Rkqs8OJSG8z4QznJbyy/view?usp=sharing

- Aula08 29/05/20 - https://drive.google.com/file/d/10DAdvf9fJx04aoz0vpfWeKK0oT7K8vpt/view?usp=sharing

- Aula09 05/06/20 - https://drive.google.com/file/d/1D737QeewEPxa9iTV7XARUwV22v9Pwsif/view?usp=sharing

- Aula10 19/06/20 - https://drive.google.com/file/d/117qgL7HN25YzItavkQm8LA9Z57q-cyQC/view?usp=sharing

- Aula11 26/06/20 - https://drive.google.com/file/d/1hCCEEEl75a0nuwn8l52oTFqBOmnQoUKe/view?usp=sharing

- Aula12 03/07/20 - https://drive.google.com/file/d/1RzOF8D1m6F4eZPwDyDH26HVcAFpasZUJ/view?usp=sharing

- Aula13 10/07/20 - https://drive.google.com/file/d/1HX1UmjykazWGOtTLt3_q1knYkVYag2FU/view?usp=sharing

- Aula14 17/07/20 - https://drive.google.com/file/d/1eWgq85L6G6-anK1W4bojcdzM84GF_xvi/view?usp=sharing

- Aula15 24/07/20 - https://drive.google.com/file/d/1VYH5Ttz9j9WaDt0n5hL2UDnE31Iir3x-/view?usp=sharing

# Projetos dos Alunos 2021

## Projetos dos Alunos 2020

- Processador do Breno Cunha Queiroz: https://github.com/Brenocq/MyMachine

-  Assembly Tron - Guilherme Amaral Hiromoto, Paulo Matana da Rocha, Caio Marcos Chaves Viana, Dennis Lenke Green - https://github.com/guilhermehiromoto/Assembly-Tron

- Pacmario - Luana Terra do Couto, Larissa freire de Jesus Costa - https://github.com/LuTDC/lab_orgcomp

- Atravesse o Mapa - Luca Porto, Leonardo Meireles - https://github.com/LucaPort0/Assembly-ICMC-Game

- Space Hit - Isadora Carolina Siebert, Marlon José Martins - https://github.com/mjmartins11/SpaceHit-Assembly

- Frogger - David Souza, Gabriel Toschi, Matheus Alves - https://github.com/bicanco/SSC0119-2020-1

- RNAssembly - Anderson Cardoso Gonçalves - https://github.com/ansoncg/RNAssembly_LabOrgComp

- HamsterDuet - Diany Pressato, Matheus da Silva Araujo - https://github.com/di-press/HamsterDuet

- Processador MASM - Eduardo Souza Rocha - https://github.com/Edwolt/Processador-MASM

- Processador de Videogame - Nathan Rodrigues de Oliveira - https://github.com/NathanTBP/NTBProcessor

- Space Race - Tyago Yuji Teoi - https://github.com/Tyago-Teoi/Projeto-de-Pr-ticas-de-Organiza-o-de-Computadores



## Projetos dos Alunos 2019

- Jogo da chuva de letras: Felipe Guilermmo Santuche Moleiro - https://github.com/FelipeMoleiro/ProjetoLabOrgArqComp

- Rex Scape: Caio Augusto Duarte Basso, Gabriel Garcia Lorencetti, Giovana Daniele da Silva, Luana Balador Belisario - https://github.com/gabrielgarcia7/game-rexscape

- Space Laser: Gustavo Vinicius Vieira Silva Soares - https://github.com/gsoares1928/ProjLabOrgArq

- Labyrinth Snake: Julia Carolina Frare Peixoto, Luís Eduardo Rozante de Freitas Pereira, Maurílio da Motta Meireles - https://github.com/LuisEduardoR/Processador-ICMC

- Snake: Gabriel Mattheus Bezerra Alves de Carvalho, Wallace Cruz de Souza - https://github.com/GabrielBCarvalho/Snake-Assembly

- Pencil: Marcelo Isaias de Moraes Junior - https://github.com/MarceloMoraesJr/JogoOrgComp

- Battle of Tiers: Jonas Wendel Costa de Souza - https://github.com/4Vertrigo/BattleOfTiers

- Bandersnack - A Dora's Text Adventure: Igor Lovatto Resende, Lucas Mateus M. A. Castro, Marcelo Temoteo de Castro - https://github.com/Beadurof/dora-machineworks

- Tic-Tac-Toe-Assembly-ICMC: NOMES: Guilherme Holanda Sanches, Lucas Henrique Rodrigues, João Vitor VIllaça - https://github.com/holondo/Tic-Tac-Toe-Assembly-ICMC
