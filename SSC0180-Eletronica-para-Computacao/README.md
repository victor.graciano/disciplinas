# Eletronica-para-Computacao
Material Usado na disciplina de Eletrônica para Computação


# Disciplina: SSC0180 - Eletrônica para Computação


## Apresentação da Disciplina

- Créditos Trabalho: 	2
- Tipo: Semestral
- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@icmc.usp.br  (Obs.: só tem um @)
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável



# Passarinho Experto! - A Prova de recuperação será explicar o que aconteceu!!

![Optional Text](../SSC0180-Eletronica-para-Computacao/MaterialAulaDistancia/PassarinhoExperto.jpg)

# Monitor
- Gabriel: gabrielalveskuabara@usp.br   (forme um grupo com as mesmas dúvidas, se possível, e combinem um horário de atendimento com o Monitor por email)

# Alunos de 2021 - Primeiro semestre

- Lista de Presença - Favor assinar a Lista de Presença durante o horario das aulas 
  - TurmaA (8:10h) - https://docs.google.com/spreadsheets/d/1hufos3XAKfLsR2-cLlIan2ynrC4B8EHbVAUek2QNhRo/edit?usp=sharing
  - TurmaB (10:10h) - https://docs.google.com/spreadsheets/d/1hkKuOzcvel3KFJDaW8_-I5OuPbwNC7BMwdmjFC9MXno/edit?usp=sharing

- Você pode acompanhar sua Frequência aqui:
  - TurmaA (8:10h) - https://docs.google.com/spreadsheets/d/1DKelZpk77Qq4Dklz1FOJRDfYSKqsfB6jLBxlLsfdNGw/edit?usp=sharing
  - TurmaB (10:10h) - https://docs.google.com/spreadsheets/d/1vlnWZl23ED8jatcMj3BhlcLeR00VqG8HqKPlEqtNW7g/edit?usp=sharing
- As aulas começam dia 20/04/2021 e serão transmitidas pelo google Meet sempre com os mesmos links:
  - Link para as Aulas - TurmaA (8:10h) - Link para o Google Meet: https://meet.google.com/mgd-gfci-ctg
  - Link para as Aulas - TurmaB (10:10h) - Link para o Google Meet: https://meet.google.com/kck-yfeq-ubd

- As aulas serâo gravadas e disponibilisadas nessa plataforma

# AVALIAÇÃO: Trabalhos turma 2021
- A availiação dos alunos será por meio da apresentação de DOIS trabalhos práticos: T1 e T2
- A nota será calculada: Nota Final = (Nota T1 + Nota T2) / 2
- Devido às restrições da Pandemia, o Trabalho 2 será OPCIONAL: Quem optar por NÃO fazer o trabalho2, terá Nota Final = Nota T1

## Inserir os dados do trabalho com link para o github/gitlab no DOC: https://docs.google.com/document/d/16EIs_e-nDZGPOZV2DnknO5rRO8fgUL7S4aaZX1Tr0z8/edit?usp=sharing

## TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

## TRABALHO1: Projeto de uma Fonte de Tensão ajustável entre 3V a 12V com capacidade de 100mA
### Incluir no seu GIT: 
- Diagrama da Fonte com os valores dos componentes escolhidos e uma lista de componentes, contendo valores e preços
- Link para o circuito no Falstad ou outro simulador
- Projeto do Esquemático e do PCB no EAGLE
- Se montou o circuito, incluir fotos da placa/protoboard
- Incluir um VIDEO mostrando o Projeto funcionando e/ou simulando, e explicando porque escolheu os valores dos componentes (Upa o vídeo no Youtube ou google drive e poe um link no Readme do teu Github/gitlab).
- Dicas de como fazer o README do github: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!
- Link para apresentação do Trabalho 1: link a ser informado
- Exemplo do projeto da fonte: https://github.com/bmarquescost/Projeto-Eletronica-USP

## TRABALHO2: Projeto de Software e Hardware com Arduino/ESP32
### Incluir no seu GIT: 
- Software; 
- Circuito com TODAS as conexoes com o Arduino/ESP32; 
- Fotos do circuito; 
- Incluir um VIDEO mostrando o Projeto funcionando e/ou simulando, e explicando porque escolheu os valores dos componentes (Upa o vídeo no Youtube ou google drive e poe um link no Readme do teu Github/gitlab).
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

# Aulas a Distância 2021

## Roteiro do conteúdo a ser coberto em 2021:
- 1.-	Overclock
  - 1.1-	Aquecimento, refrigeração
  - 1.2-	Operação de uma CPU
  - 1.3-	Lei de Ohm
  - 1.4-	O transistor como chave - MOSFET
  - 1.5-	Construção de portas lógicas com transistores
  - 1.6-	Atrasos lógicos na propagação de sinais
  - 1.7-	Capacitores
  - 1.8-	Fan-out


- 2-	Projeto de Fonte de tensão regulada
  - 2.1-	Transformador
  - 2.2-	Diodo, ponte de diodo
  - 2.3-	Diodo Zener
  - 2.4-	O transistor como amplificados – transistor NPN
  - 2.5-	Regulador de tensão – LM317
  - 2.6-	A ferramenta EAGLE
  - 2.7-	Fabricação de Placas de circuito impresso


- 3-	Co-Projeto de software e hardware
  - 3.1- Arduino – ferramentas e programação
  - 3.2- ESP32 – ferramentas e programação
  - 3.3- Circuitos periféricos – sensores e atuadores


## Aula 01 (20/04/2021)
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/1wn8J-cT-2GpBkJmlJOw031ttQHadJ72Z/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/1OEPuropkfiPIYZQfiduQqQe0psAQS38a/view?usp=sharing

## Aula 02 (27/04/2021
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/19qLoA0pH6jZ0KKRxnkiV1lRi9hu8kPvv/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/1UU5cT2hWadDnmi8H8MkssX122tN5WRui/view?usp=sharing
- Konrad Zuse - http://www.geocities.ws/hifi_eventos/Z1.html
- Arquitetura do nosso Processador ICMC - https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0180-Eletronica-para-Computacao/SSC0511-Organizacao-de-Computadores-Digitais_instrucoes_10_2016.pdf
- Material (Fotos da Lousa, PDFs...) - https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0180-Eletronica-para-Computacao/MaterialAulaDistancia

## Aula 03 (04/05/2021)
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/1kzfpJaAuXWM3EwphyzxasgxiL4kn8kEk/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/1kHI9TJiGeH2vQ736ogS18lm2ddlWqeIm/view?usp=sharing
- TINKERCAD - https://www.tinkercad.com/
- Porta NOT (nMOS e CMOS) http://tinyurl.com/uk6az46
- Liga LED e tensão de ativação do transistor - https://tinyurl.com/yhbob6lz
- Material (Fotos da Lousa, PDFs...) - https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0180-Eletronica-para-Computacao/MaterialAulaDistancia

## Aula 04 (11/05/2021)
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/11tZEn3MgVQc6Oh_UO64dkzFbvej2k48y/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/1EF_kj930BpFy3DHttsvnssoUBK6xCb_P/view?usp=sharing
- Porta NOT (nMOS e CMOS) http://tinyurl.com/uk6az46

## Aula 05 (18/05/2021)
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/1hWICmsh-yQSZQcWFEEvpK3Di6TbKMxpy/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/1ME-LBg67L1NYmp17fTBDjBSB4B7_UswW/view?usp=sharing
- Porta NOT (nMOS e CMOS) http://tinyurl.com/uk6az46
- Teste de Tensão Trifásica e Lei de Kirchhof - https://tinyurl.com/yhqjc33s
- Vídeo explicando Tensão RMS - https://www.youtube.com/watch?v=efKo595btKY
- Cálculo da Tensão RMS - http://mundoprojetado.com.br/tensao-eficaz-ou-rms/

## Aula 06 (25/05/2021)
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/1m3PB4YHxW7hgaYl1S6fiNtxd5BnkaKaE/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/103amLa9udglX4FlEOJwwZpxx-uAVQbJC/view?usp=sharing

## Aula 07 (01/06/2021)
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/1Fx_74629R6UWswnBFumsrB9aFgWmle89/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/1RdgYu779-XLeFQJwDXloS_e_ec4naHwW/view?usp=sharing

## Aula 08 (08/06/2021)
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/165aeNnkzAZHzyoLHCzNzHgBiakkmYRD3/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/1iN2hmuY8L0_eTcRhT_lVhOolclVy-cdh/view?usp=sharing
- Capaitor - https://tinyurl.com/y6tyo82m
- How to cook hotdogs with a 20000 volt capacitor bank - https://www.youtube.com/watch?v=DQ67njnNaxw

## Aula 09 (15/06/2021)
- Link para a Aula - TurmaA (8:10h) - https://drive.google.com/file/d/1izvacNa27Sbr2Zng8SH4Bg7qI8fbzVvI/view?usp=sharing
- Link para a Aula - TurmaB (10:10h) - https://drive.google.com/file/d/1izvacNa27Sbr2Zng8SH4Bg7qI8fbzVvI/view?usp=sharing
- Cadeia NOT FANOUT(nMOS) http://tinyurl.com/sjp8cq8
- Cadeia NOT FANOUT(CMOS) http://tinyurl.com/vke6tj7
- Cadeia NOT FANOUT 5V-10V (CMOS) http://tinyurl.com/ua8m7xh

## Aula 10 (22/06/2021)
- Link para a Aula - TurmaA (8:10h) - Link para o Google Meet: https://meet.google.com/mgd-gfci-ctg
- Link para a Aula - TurmaB (10:10h) - Link para o Google Meet: https://meet.google.com/kck-yfeq-ubd


# Aulas a Distância 2020

## Aula 01
- Link para a Aula 01 (Primeira parte) - https://drive.google.com/open?id=1VUf9hpZY_A0Y52DMYKQc3CpMpyAdmc0R
- Link para a Aula 01 (Segunda parte) - https://drive.google.com/open?id=1yGJSxwicxm9Ieqwy1j5SGRqh1RDHzaBs
- Porta NOT (nMOS e CMOS) http://tinyurl.com/uk6az46
- Material (Fotos da Lousa, PDFs...) - https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0180-Eletronica-para-Computacao/MaterialAulaDistancia

## Aula 02
- Link para a Aula 02 (31/03 - 9h-11h)(Primeira parte)- https://drive.google.com/file/d/1f1nJomHC0IRB5MCO075ggAsGgK87DZQn/view?usp=sharing
- Link para a Aula 02 (31/03 - 9h-11h)(Segunda parte)- https://drive.google.com/file/d/1CDAUlr57fzFmY3mvgLYqeRr07TBzYI2t/view?usp=sharing
- Cadeia NOT FANOUT(nMOS) http://tinyurl.com/sjp8cq8
- Cadeia NOT FANOUT(CMOS) http://tinyurl.com/vke6tj7
- Cadeia NOT FANOUT 5V-10V (CMOS) http://tinyurl.com/ua8m7xh

## Aula 03
- Link para a Aula 03 (14/04 - 10h-12h) - https://drive.google.com/file/d/1Wpkuvzy795SuMsvXzifPzCMcMF_9v_UA/view?usp=sharing
- Porta AND (nMOS) http://tinyurl.com/ukexy4p
- Porta OR (nMOS) http://tinyurl.com/tkajd8r
- Porta AND (CMOS) http://tinyurl.com/wd6xpb7
- Porta OR (CMOS) http://tinyurl.com/rp6qbq5

## Aula 04
- Link para a Aula 04 (28/04 - 10h-12h) - 1a Parte: https://drive.google.com/file/d/1b-37zbzwszkNOXv95_sQ804Or-ZhBOk2/view?usp=sharing - 2a Parte: https://drive.google.com/file/d/1pFw3IaXov-yPAh5_ijqYIbNekv6GG3c8/view?usp=sharing
- Porta AND (RELÉS) http://tinyurl.com/yd5ueskf
- Porta XOR (nMOS) http://tinyurl.com/y89d5keo   by Gabriel Vicente Rodrigues
- Porta XOR (CMOS)  http://tinyurl.com/ycw7vqxh  by Gabriel Vicente Rodrigues
- Porta XOR (OU, NAND, AND)   http://tinyurl.com/y9cgrgzr  by Giovanni Shibaki Camargo
- Cadeia NOT ERRADA (NPN) http://tinyurl.com/y2mxmgwj
- Cadeia NOT ERRADA (nMOS) http://tinyurl.com/y6le5sgn
- Maneira correta de usar Transistores para ligar Motores, Lampadas e buzinas... http://tinyurl.com/yba97xae

## Aula 05
- Link para a Aula 05 (5/05 - 10h-12h) - https://drive.google.com/file/d/1xg4D_HuzDNN-FNQL5oQA68Z4KvzuhLrq/view?usp=sharing
- Transistor NPN - http://tinyurl.com/y9j4tcad

## Aula 06
- Link para a Aula 06 (12/05 - 10h-12h) - https://drive.google.com/file/d/12mdVjGn8GEB2rgISNXUEkLL9uuzCK3BZ/view?usp=sharing
- Fonte1 (Trafo-led) http://tinyurl.com/yahh7trl

## Aula 07
- Link para a Aula 07 (19/05 - 10h-12h) - https://drive.google.com/file/d/1j7ZVFB5tNXEpjdvk_EKpp-4trSBhOjix/view?usp=sharing
- Fonte1 (Trafo-diodos-capacitor-led) http://tinyurl.com/y5793zjk

## Aula 08
- Link para a Aula 08 (26/05 - 10h-12h) - https://drive.google.com/file/d/13jxRI0ZMFKEQE402T1keGWhe6QTZR2VJ/view?usp=sharing
- Fonte1 (Trafo-diodos-capacitor-led-Zenner) - http://tinyurl.com/yd2vhkgt

## Aula 09
- Link para a Aula 09 (02/06 - 10h-12h) - https://drive.google.com/file/d/1zqqEuqQ6va3YZKNoUxRowhwQA6U7k6rH/view?usp=sharing
- Fonte1 (Trafo-diodos-capacitor-led-Zenner-Regulador_NPN) - http://tinyurl.com/ybku2grm
- Fonte1 (Trafo-diodos-capacitor-led-Zenner-Regulador_NPN-High_Current) - http://tinyurl.com/y8ewpw2f

## Aula 10
- Link para a Aula 10 (10/06 - 10h-12h) (Primeira parte) - https://drive.google.com/file/d/1ct7Try-ZIL3kkUo1eTyypNPazq5tpNES/view?usp=sharing
- Link para a Aula 10 (10/06 - 10h-12h) (Segunda parte) - https://drive.google.com/file/d/1pvzKOBKydxEaICAXnQBZqKUzdQ3KnTv3/view?usp=sharing
- Fonte1 (Trafo-diodos-capacitor-led-Zenner-Regulador_NPN_VU_Leds) - http://tinyurl.com/ybklydge

## Aula 11
- Link para a Aula 11 (16/06 - 10h-12h) - https://drive.google.com/file/d/16L_mBfgHGEHAN8DW2E6BVh118TDAtzYp/view?usp=sharing
- Dicas do EAGLE: Ver mais acima em Dicas do EAGLE

## Aula 12
- Link para a Aula 12 (23/06 - 10h-12h) - https://drive.google.com/file/d/1BctQ3Tzq_16EbLE4qSp4GiHg0cpB4YVi/view?usp=sharing

## Aula 13
- Link para a Aula 13 (30/06 - 10h-12h) - https://drive.google.com/file/d/1T-WCInZrVZc_vT5ZgheijIoFCT7ITfVD/view?usp=sharing
- Tutorial Fazer placa com canetinha e ferro de passar usando Percloreto de Ferro => Cloreto Férrico(FeCl3) + Cobre(Cu) = Cloreto Férrico(FeCl2) + Cloreto Cúprico(CuCl2) - https://www.youtube.com/watch?v=P08uX38rr7o
- Tutorial Fazer placa com ferro de passar e Papel Glossy - https://www.youtube.com/watch?v=daEE4iZ-U8A
- Tutorial Fazer placa com tinta fotosensível - https://www.youtube.com/watch?v=q8ludS_A3xo
- Reaproveitamento do Percloreto de Ferro - https://www.youtube.com/watch?v=hTQyt1hPSx8
- Cuidados e proteção individual - https://www.aquario.pt/Imgs/banners/banner_14/Ficha%20de%20Seguranca%20Percloreto%20de%20ferro.pdf

## Aula 14
- Link para a Aula 14 (07/07 - 10h-12h) -  https://drive.google.com/file/d/1VVH_VVLbFXTyin1Cwnf6T60cMsyowXD2/view?usp=sharing
- Tutorial do tinkercad.com    -  https://www.tinkercad.com/

## Aula 15
- Link para a Aula 15 (14/07 - 10h-12h) - https://drive.google.com/file/d/1_dbDqdmQBaXzmVPwOdwPvqSqO-UnUaSS/view?usp=sharing

## Aula 16
- Link para a Aula 16 (21/07 - 10h-12h) - https://drive.google.com/file/d/1HkCjVSIuclEu9f5VOM7ndT67zwUeKgrr/view?usp=sharing

## Aula 17
- Link para a Aula 17 (28/07 - 10h-12h) - https://drive.google.com/file/d/1EN6wZylCkJEI915RjKQmbbztAkaza-gc/view?usp=sharing

# Dicas do EAGLE:
- Fica esperto quando ele apresenta a caixa deresultado do Autorouter, o primeiro item geralmente NAO dá certo,mais vários mais abaixo conseguem 100%.
- Depois de fazer uma PCB e rotea-la, as vezes precisamos fazer mudanças no Schematico e isso vai gerar insconsistências (Raiozinho fica RED no canto inferior direito)!!
- Para manter a consistëncia das ligações: Vai no PCB e UNROUT ALL -> Ripup->AllSignal; Dai no Schemático->File->Export->NetScript; Depois vai no PCB e roda o SCRIPT ->File->ExecuteScrip (Tutorial - https://www.youtube.com/watch?v=m1gjuE5i47I)
- Pra manter a consistência dos Valores: Vai no Schematico e roda um script chamado "Make SCH/BRD consistent by values" (make-value-consistent) -> Ele roda ERC, exporta os valores e importa no  PCB (Tutorial - https://www.youtube.com/watch?v=rsacV5SQkVM)
- Na hora de imprimir o circuito, não esqueça de imprimir o BOTTOM em MIRROR
- Sempre escolha componentes com FOOTPRINT
- Sempre preencha os VALUES dos componentes antes de ir pro PCB

- Aula Tutorial EAGLE Parte 1: https://www.youtube.com/watch?v=jCn-QlsnRMU

- Aula Tutorial EAGLE Parte 2: https://www.youtube.com/watch?v=TP7zXqjaEfo

- Screan Captured: https://www.youtube.com/watch?v=qb6fcfDH5Qk&t=2s

- Screan Captured: https://www.youtube.com/watch?v=vQig6pRj4C4


# Dicas do ARDUINO:

Eu desinstalei a versao que tava (Arduino2:1.0.5) e baixeidireto do GITHUB a ultima versao do Arduino= 1.8.5
link: https://www.arduino.cc/en/Main/Software
Deszipar a pasta e executar o install.sh pra instalar o sw.

MOVA de Downloads para Documentos ou para HOME, senao o retardado instala no Downloads!
Para deszipar (Usar TAR mesmo, pois deixa as permissoes certas!!):
tar -xvf arduino-1.8.9-linux64.tar.xz 

Para instalar: 
 sudo ./install.sh

Para arrumar as permissoes das pastas PARA O USUARIO NORMAL (se nao, ele vai ficar enchendo o saco pra executar como SUDO), executar:  
./arduino-linux-setup.sh $USER


--> Essa versao funcionou e programaou o Arduino de primeira, pois eu ja' tinnha resolvido os problemas da USB na versao anterior...

LIBRARIES: /home/simoes/Arduino/libraries/
--> Mudou as libraries pra esse novo lugar!!

USB PROBLEMS --> So' que agora nao vai encontrar a porta serial pra programar o arduino!
--> Tem que arrumar assim:

lsusb  --> Lista tudo que esta' instalado nas USBs, se conectar o cabo e dar o comando novamente vai mostrar a unica diferente, que e'  o arduino!
--> Isso mostra se ja' tem um driver instalado pro chip de USB da placa do Arduino! Geramelte tem no Ubuntu.

ls /dev  --> Lista tudo que esta' instalado na maquina, geralmente o arduino e' a ttyUSB0, se nao tiver nada mais ligado nas USB
ls -l /dev/ttyUSB0
	crw-rw---- 1 root dialout 188, 0 Mai  9 12:44 /dev/ttyUSB0
--> Isso mostra que o ttyUSB0  faz parte do grupo dialout

--> Agora e'  so'  adicionar nosso usuario ao grupo dialout
sudo usermod -a -G dialout simoes     --> onde simoes e' seu username !!
--> Tem que se deslogar e logar novamente pra fazer essas mudancas!!


ISSO NAO RESOLVE NO Ubuntu 18.04, pois da' pau no JAVA!
==> Fica dando essa mensagem quando abre o arduino: java.lang.NullPointerException thrown while loading gnu.io.RXTXCommDriver

--> Instalar Java:
apt-get install openjdk-8-jre

--> On a newly installed Ubuntu 18.04 LTS system. After installing Oracle java 8:

$ sudo add-apt-repository ppa:webupd8team/java
$ sudo apt update
$ sudo apt install oracle-java8-set-default

DAi' FUNCIONOU!!!



# Projeto 1 (2020) - Fonte:

- Grupo 1 (Gabriel Souza, Rafael Tavares): 
https://github.com/gsasouza/fonte

- Grupo 2 (Matheus H. de C. Pinto, Pedro Castro, Gustavo Brunelli):
https://github.com/cerqueiramatheus/Fonte-de-Tensao

- Grupo 3 (Giovanni S. Camargo, Pedro K. M. Carmo, Melissa M. Nogueira):
https://github.com/giovanni-shibaki/SSC0180_2020_Fonte_Tensao

- Grupo 4 (Bernardo M. Costa,Gabriel F.X. Vasconcelos,Pedro Augusto R. Gomes):
https://github.com/bmarquescost/Projeto-Eletronica-USP

- Grupo 5 (Pedro H B Monici, Tulio S Ramos, Gabriel Dertoni, Eduardo H P Silva):
https://github.com/pedromonici/SSC0180---Eletronica-para-Computacao

- Grupo 6 (João V. Sene, João A. Misson, Vitor C. Brustolin):

- Grupo 7 (Matheus V. Gonçalves, Pedro H. D. J. de Souza):
https://github.com/MatthG42/Fonte-de-Tensao-3-12v

- Grupo 8 (Milena C. da Silva, Lourenço de S. Roselino, Marco A. Toledo):
https://github.com/milenacsilva/Eletronica-Projetos/tree/master/Fonte

- Grupo 9(Gabriel V Rodrigues, Lucas Mihara, Natan Sanches, Vitor Beneti):
https://github.com/natan-dot-com/Fonte-Eletronica

- Grupo 10 (Pedro Afonso Perez Chagas, Gabriela Satie Faria Nishimi):
https://github.com/pedrousp/Eletronica/tree/master/Fonte

- Grupo 11 (Pedro Henrique Raymundi):
https://github.com/PedroRaymundi/fonte-de-Tensao

- Grupo 12 (Joao Pedro G. Favoretti, Lucas Pilla Pimentel):
https://github.com/Lucas-Pimentel/Fonte-de-Tensao

- Grupo 13 (Guilherme Pacheco, Thiago Cardoso, Pedro Martelleto, Ciro Falsarella):
https://github.com/copach/fonte 020

- Grupo 14 (Victor Paulo Cruz Lutes):
https://github.com/VictorLutes/Fonte-Eletronica-SCCO180 

- Grupo 15 (Pedro Henrique Conrado, Leonardo Chieppe)
https://github.com/PedroConrado/Fonte-eletronica

- Grupo 16 (Alcino Salviano Cavalcanti): https://github.com/Alcino-Cavalcanti/Eletronica/tree/master/ProjetoFonte

- Grupo 17 (João Lucas R. Constantino, Gabriel Z. Manhani):
https://github.com/jlrconstantino/fonte-de-tensao

- Grupo 18 (Ana Vitória, Eduardo Amaral, Sofhia Gonçalves, Matheus silva):
https://github.com/sofhiasouza/Projeto-Fonte-de-Tensao

- Grupo 19 (Adrio O. Alves, Eduardo V. B. Rossi, Felipi Y. Santos, Lucas F. de Almeida): https://github.com/lucasalmeida-pd/fonte-100mA

- Grupo 20 (João Francisco Caprioli Barbosa Camargo De Pinho):
https://github.com/JotaGHz/FonteEletronica

- Grupo 21 (Felipe Cadavez Oliveira, Vinícius Santos Monteiro, Mateus Silva Cruz)
https://github.com/CadavezUSP/Eletronica

- Grupo 22 (Lucas Gabriel M. Miranda):
https://github.com/lucasg-mm/fonte_de_tensao 

- Grupo 23 (Guilherme Ramos Costa Paixão):
https://github.com/gp2112/Fonte-127vac-12vdc

- Grupo 24 (Diógenes Silva Pedro, Rodrigo Lopes Assaf, Pedro Liduino do Nascimento):
https://github.com/DioUSP/Trabalho-Eletronica-Fonte

- Grupo 25 (João Marcos Cardoso da Silva)
https://github.com/JoaoMarcosCSilva/Fonte-3-12V

- Grupo 26 (Alexandre Lima Palles Rocha, Matheus Barcellos de Castro Cunha): https://github.com/matheushw/SSC0180-Fonte

- Grupo 27 (Matheus Ventura de Sousa, João Guilherme J. Marinho)
https://github.com/matheusvdes007/Fonte-Eletronica

- Grupo 28 (Thamyres Santos Silva, Gabriela Rodrigues do Prado): https://github.com/thamysis-usp/Projeto-Fonte-Zenner.git

- Grupo 29 (Israel Felipe da Silva): https://github.com/israelfelipe9/Projeto-Fonte

- Grupo 30 (Breno Lívio Silva de Almeida):  https://github.com/BrenoUSP/SSC0180_2020_Fonte

- Grupo 31 (Elisa Rachel Beninca Martins, Igor Mateus Queiroz Gato, Mateus dos Santos Ribeiro, Jonatas A Lopes):  
https://github.com/IgorGato/Fonte-de-Tensao-Ajustavel-3-12V

- Grupo 32 (Marcos Patrício Nogueira Filho): 

- Grupo 33 (Beatriz Aparecida Diniz): https://gitlab.com/beatrizdiniz/fonte

- Grupo 34 (Fernando Henrique Paes Generich): https://github.com/FerHPGene/Fonte-de-Tens-o-3-12V-100mA/blob/master/README.md

- Grupo 35 (Lucas Massao Fukusawa Dagnone): https://github.com/DagN1/Trabalho-SCC 

- Grupo 36 (Eduardo Garcia de Gáspari Valdejão): https://github.com/EduardoGDGV/Fonte-de-Tensao

- Grupo 37 (Yann Amado Nunes Costa):  https://github.com/YannAmado/Eletronica_Comp

- Grupo 38 (Thales Darini Souza): https://github.com/thalesdarini/projeto-eletronica-fonte 

- Grupo 39 (Gustavo Lelli, Ana Cristina Silva de Oliveira, Matheus Bermudes Viana): https://github.com/gustavo-lelli/Fonte-de-Tensao

- Grupo 40 (Gabriel Kuabara, Guilherme Rios, Guilherme Toledo, Victor Henrique de Sa Silva):  https://github.com/GKuabara/Projeto-Eletronica-USP

- Grupo 41 (Leonardo Dallagnol): https://github.com/Leo-LNRD/Fonte

- Grupo 42 (Luísa Balleroni Shimabucoro, Raissa Torres Barreira, Wictor Dalbosco Silva): https://github.com/WictorDalbosco/Fonte-de-Tensao/

- Grupo 43 (Bernardo Azrak Nardelli): https://github.com/bzrak/eletronica2020/tree/master/Fonte

- Grupo 44 (Gabriel Victor, Alexandre Brito): https://github.com/gabrielvictorcf/Fonte-Eletronica

- Grupo 45 (Rafael Corona): https://github.com/Rafael-Corona/Trabalho1_fonte_SCC0180

- Grupo 46 (Álvaro José Lopes, Paulo Soares): https://github.com/Alvaro-dev/FonteTensaoVariavel

- Grupo 47 (Luiz Fernando Rabelo): https://github.com/luizfernandorabelo/Graduacao/tree/master/1-%20%20Eletr%C3%B4nica%20para%20Computa%C3%A7%C3%A3o/Projeto%20I%20-%20Fonte

- Grupo 48 (Leonardo Chagas Pizzo):  https://github.com/lcpizzo-usp/Fonte-de-Tensao

- Grupo 49 (Érica Ribeiro F. dos Santos): https://github.com/ericarfs/Projeto-Fonte-de-Tensao

- Grupo 50 (Osni Brito): https://github.com/StrenjaX/Projeto-fonte-com-tens-o-ajustavel-3V---12V-



## Projeto 2 (2020) - Arduino:

- Automação Residencial (Gabriel Souza, Rafael Tavares): https://github.com/gsasouza/house-automation

- Carrinho que desvia de obstáculos (Luiz Fernando Rabelo): https://github.com/luizfernandorabelo/Graduacao/tree/master/1-%20%20Eletr%C3%B4nica%20para%20Computa%C3%A7%C3%A3o/Projeto%20II%20-%20Arduino







# Projeto Alunos 2019
- Disco-Sound-Beat-Led-RGB (Lucas Gabriel, Mateus Penteado, Daniel Suzumura, Frederico Bulhoes): https://github.com/LucasGab/Led-Controller
- Sistema de desbloqueio por senha (Fernando Lincoln, Victor Kendi, Leonardo Gabriel): https://github.com/lincolncpp/senha-controle-remoto
- Automação Residencial (Gabriel Souza, Rafael Tavares): https://github.com/gsasouza/house-automation
- Jogo da Velha (Thiago Sena, Victor Oliveira): https://github.com/Thiago-SDQ/jogo_da_velha
- Arduino Genius Game (Dennis Green, Pedro Cisdeli, Pedro Lourenço): https://github.com/Haltz01/Arduino_Genius
- Automação via Bot do Telegram (Gustavo Bartholomeu Trad Souza): https://github.com/gustavobartho/Automacao_Residencial_Telegram_Bot
- 2D Penter - Impressora 2d para plotar gráficos (Breno Cunha Queiroz, Dikson Ferreira): https://github.com/Brenocq/2DPenter
- Bomberman (André Santana, Gabriel Ferracioli, Kevin Naoto): https://github.com/AndreSFND/Bomberman-Arduino
- Arduino controlado por Node.js (Leonardo Antonetti, Diogo Castanho Emídio, Jorge Augusto Salgado): https://github.com/l-a-motta/arduino-node
- Fechadura Eletrica via WebServer (Joao Pedro Berno Zanutto): https://github.com/zanuttin/Fechadura-WebServer
- Jogo de pong (Ana Clara Amorim, Kaio Tadeu Rodrigues): https://github.com/AnaClaraAmorim/eletronica-para-computacao
- Sensor de temperatura (Gabriel Nicolau, Daniel Froes, Guilherme, Mateus Bragança): https://github.com/7Nic/Sensor-de-temperatura
- Robô Omnidirecional (Lucas Romera, Maria Eduarda Kawakami, Otto Fernandes) https://github.com/ottusp/Eletronica_Arduino
- Medidor de Temperatura e Umidade (César Guibo, Leonardo Rebelo, Ricardo Nascimento): https://github.com/Rebelo/medidor_temp_umidade
- Xiloino - um instrumento musical (Paulo José, Jonathan Sanchez, Augusto de Elet): https://gitlab.com/ImperfectFractal/xiloino
- Leitor de Morse por luz (Anderson Cardoso, Bruna Garcia, Hellen Rosa, Mateus de Souza): https://github.com/OhMySt4rs/LeitordeMorse
- Exibição de mensagem em matriz de leds 8x8 (Marcus Vinícius Medeiros Pará, Luca Machado Bottino): https://github.com/MarcusMedeiros99/max7219
- Simon42-Game (Bruna Magrini, Gustavo Akira, Marlon Martins, Wellington Amaral): https://github.com/mjmartins11/projectsimon42
- Tradutor de Codigo Morse (Luis Otavio M. Ferreira, Leonardo F. Pinheiro, Luca M. Alexander, Maria Fernanda L. de Mello): https://github.com/LuisOtavioMF/SCC0117-Trabalho2
- Rolador de Dados, com Seleção de dados (Guilherme Hiromoto, João Victor de Araujo Lima, Murilo Franchi Paulo Matana da Rocha, Vinicius Shimizu): https://github.com/mrlFranchi/DiceRoller
- Genius Game With Sensor (Gabriel Rossati, Nathan Rodrigues, Yure Pablo): https://github.com/yp1plus/geniusWithSensor_game
- Piano Hero (Bruno Fernandes Moreira, Francisco Pedrosa, Savio Duarte Fontes, Thales Willian Dalvi da Silva): https:https://github.com/FranPedrosa/PianoHero<br/>
- Arduino Robot Car (Luca Batista Barbosa, Mylena Vieira Pim): https://github.com/mylenapim/Arduino-Robot-Car
- Tradutor para Código Morse (Daniel Vieira, Vinícius Kuhlmann, Luis Rodrigues): https://github.com/Idalen/Arduino_Morse_Translator
- Termômetro Digital (Eduardo Souza Rocha, Fabio Dias da Cunha): https://github.com/Edwolt/Termometro-Arduino/
- Detector de Porta Aberta (Fernando Cincinato, Olavo Morais): https://github.com/FernandoCincinato/Detector-de-porta-aberta-Arduino
- Arduino Pong Game (Diany Pressato, Larissa Freire, Luana Terra do Couto, Matheus Araujo, Tyago Teoi): https://github.com/M-A-rs/Arduino-Pong-Game
- Sistema de monitoramento de umidade do solo (André Vieira, Leandro Satoshi): https://github.com/LSatoshi/Soil_Moisture_Monitoring
- Dado-inator (Bernardo Andrade, Filipe Oliveira, Victor Russo, Yasmin Araújo): https://github.com/vrrusso/dado-inator
- Labirinto Transdimensional em primeira pessoa (Dalton Sato, Marcus Martins, Vinícius de Araújo, Vitor Amim): https://github.com/marcuscastelo/arduino-transdimentional-maze
- Piano de Bananas (Jose Daniel, Isadora Siebert, Erick Barcelos, Pablo Oliviera): https://github.com/pabloolvr/Piano-de-Bananas
- Osciloscópio USB (Henrique Libutti, Matheus Borella, Natan Cerdeira): https://github.com/natanbc/stm32_usb_scope
- Medidor de Pulso (Caio Marcos Chaves Viana, Lucas Carvalho Machado, Lucas Yuiti Oda Yamamoto, Gabriel de Oliveira Guedes Nogueira, Victor Gomes de Carvalho): https://github.com/Talendar/arduino_pulse_monitor/blob/master/README.md
- Fonte da bancada (André Niero Setti): https://github.com/Setti7/eletronica


# Não Faça em Casa

<div align="left">
 <img src="/SSC0180-Eletronica-para-Computacao/MaterialAulaDistancia/catghost.jpg" height="50">
</div>

 - https://www.youtube.com/watch?v=slhocFSAoQE
 - https://www.youtube.com/watch?v=jBwodg8sfdU
 - https://www.youtube.com/watch?v=jUE1IHN6OI8
 - https://www.youtube.com/watch?v=yCwmYfkTInY

# Circuitos:

Porta NOT (nMOS e CMOS) http://tinyurl.com/uk6az46

Cadeia NOT (nMOS) http://tinyurl.com/t6z7ff4

Cadeia NOT FANOUT(nMOS) http://tinyurl.com/sjp8cq8

Cadeia NOT FANOUT(CMOS) http://tinyurl.com/vke6tj7

Cadeia NOT FANOUT 5V-10V (CMOS) http://tinyurl.com/ua8m7xh

Porta AND (RELÉS) http://tinyurl.com/yd5ueskf

Porta AND (nMOS) http://tinyurl.com/ukexy4p

Porta OR (nMOS) http://tinyurl.com/tkajd8r

Porta AND (CMOS) http://tinyurl.com/wd6xpb7

Porta OR (CMOS) http://tinyurl.com/rp6qbq5

Porta XOR (nMOS) http://tinyurl.com/y89d5keo   by Gabriel Vicente Rodrigues

Porta XOR (CMOS)  http://tinyurl.com/ycw7vqxh  by Gabriel Vicente Rodrigues

Porta XOR (OU, NAND, AND)   http://tinyurl.com/y9cgrgzr  by Giovanni Shibaki Camargo

Cadeia NOT ERRADA (NPN) http://tinyurl.com/y2mxmgwj

Cadeia NOT ERRADA!(nMOS) http://tinyurl.com/y6le5sgn

Maneira correta de usar Transistores para ligar Motores, Lampadas e buzinas... http://tinyurl.com/yba97xae

Fonte1 (Trafo-diodos-capacitor-led) http://tinyurl.com/y5793zjk

Fonte1 (Trafo-diodos-capacitor-led-Zenner-Regulador_NPN) http://tinyurl.com/ybku2grm


# Objetivos:

Familiarizar os alunos com o funcionamento eletrônico das portas e lógicas com conceitos relacionados a dispositivos eletrônicos.
 
Programa Resumido
Conceitos básicos de circuitos eletrônicos; caracterização dos componentes básicos: resistores, capacitores, diodos e transistores; projeto de circuitos eletrônicos com componentes básicos; projeto de funções lógicas com transistores: portas lógicas digitais; simulação de sinais elétricos em circuitos eletrônicos, efeitos transitórios na propagação de sinais elétricos em circuitos de lógica digital; projetos de fontes de tensão de corrente contínua.
 

Programa
Conceitos básicos de circuitos eletrônicos, com ênfase na utilização de componentes eletrônicos básicos, tais como resistores, capacitores, diodos e transistores, no projeto de circuitos e portas lógicas digitais. Uso de simuladores de circuitos elétricos para visualização de formas de onda. Estudo do efeito de fontes de tensão de corrente contínua em circuitos elétricos. Aplicações de transistores como chaves binárias no projeto de funções lógicas, como portas NOT, AND, OR e XOR. Efeitos transitórios na propagação de sinais elétricos em circuitos de lógica digital, como tempos de subida e descida, efeitos fan-out e suas formas de ondas. Circuitos NMOS e CMOS: caracterização e efeitos de variação de voltagem e frequência.
 

Avaliação
     	Método
     	Exposição seguida de exercícios e trabalhos práticos, dentro e fora de classe.
 
     	Critério
     	Teremos dois trabalhos T1 e T2 e a nota final será a média aritmética entre eles: NF = (T1 + T2 )/2, 
      sendo que é necessário obter nota superior a CINCO em ambos os trabalhos.
 
     	Norma de Recuperação
     	Critério de Aprovação: 
      NP+(Mrec/2,5), se Mrec >= 7,5; ou Max {NP,Mrec}, se Mrec <= 5,0;
      ou 5,0, se 5,0 <= Mrec < 7,5.( NP=1ª avaliação, Mrec=prova recuperação).
 

Bibliografia
Livro-texto: 
- ANNIBAL, HETTEM JR, Fundamentos de Informática – Eletrônica para Computação.

Bibliografia complementar:
- SEDRA, SMITH, Microeletrônica, Makron Books, 2000. 

1) Brophy, James John: Eletronica basica.
 Rio de Janeiro Guanabara Dois 1978.
Biblioteca:  68M05.10 B869e e.1

 
Sequência da Disciplina:

SSC-180 Eletrônica Para Computação

SSC-117 – Introdução à Lógica Digital I

SSC-118 Sistemas Digitais

SSC-112 - Organização de Computadores Digitais I
(SSC-119 – Prática em Organização de Computadores)

SSC-114 - Arquitetura de Computadores, 
